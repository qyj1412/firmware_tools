# 固件生成脚本工具





## 关于如何提取app的icon

有两种方法

**方法1**

通过代码, 但是太麻烦了懒得做



**方法2**

通过安装IconView来提取Icon

[使用方法](https://blog.csdn.net/culinluo3322/article/details/108706026)

IconView放在Tools路径下

![](RES\readmine_res\iconview.png)





## 关于文件

**bat_path.py**

这个文件里面是界面信息的配置文件, 程序启动时会读取该文件中的信息来创建界面以及运行程序



***Firmware_Path***

Firmware_Path是一个字典
字典的键是最左侧中的下拉选项, 用于切换脚本路径

![](RES\readmine_res\keys.png)



字典中的值是脚本的目录, 程序启动会提取目录下的所有cmd程序, 显示在界面中, 点击执行脚本, 此时界面会卡死, 等到脚本执行完成后再退出

![](RES\readmine_res\script.png)



***App_Tool_Path***

快速应用的路径信息

键: 显示在界面中的应用信息, 由于qml产生的字符串默认会大写, 这边就当是官方程序的一个bug

app_path: 

应用的路径, 格式必须按照`'RES//app_icon//plcupdate.png'`的结构, 并且必须是相对路径, 绝对路径中的`D://`会报错

app_icon_path:

应用图标的路径, 根据朱工的需求新增的功能, 帮助老年人能够快速找到应用并点击

---

**main.py**

启动程序



