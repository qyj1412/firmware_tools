'''
Descripttion: 
Author: QYJ
version: 
Date: 2022-09-06 13:55:47
LastEditors: QYJ
LastEditTime: 2022-12-17 14:51:32
'''
Firmware_Path = {
    "3.6": "E://git_data//XINJE_CODE//Firmware_3.6",
    "3.7": "E://git_data//XINJE_CODE//Firmware_3.7",
}

App_Tool_Path = {
    "PLC 自更新工具": {
        'app_path': r"D://XINJE//XDPPro//3.7.16_20221123.beta4//TOOL//firmware//FM_Update.exe",
        'app_icon_path': r'RES//app_icon//plcupdate.png'
    },
    "PLC DownSys工具": {
        'app_path': r"E://CompanyWeChat_data//WXWork//1688853358993261//Cache//File//2022-09//DownSys3(2)//DownSys3//XD_DownSys3.exe",
        'app_icon_path': r'RES//app_icon//donwsys.ico'
    },
    "File Exchange工具": {
        'app_path': r"E://py_data//TransFileManager//ui.py",
        'app_icon_path': r'RES//app_icon//jojo.png'
    },
    "STM32-ST-Line Utility": {
        'app_path': r"E://STLink_Utility//ST-LINK Utility//STM32 ST-LINK Utility.exe",
        'app_icon_path': r'RES//app_icon//stm32_utility.png'
    },
    "VSCode": {
        'app_path': r'D://Microsoft VS Code//Code.exe',
        'app_icon_path': r'RES//app_icon//vscode.png'
    },
    "VS2019": {
        'app_path': r'C://Program Files (x86)//Microsoft Visual Studio//2019//Professional//Common7//IDE//devenv.exe',
        'app_icon_path': r'RES//app_icon//vs2019.ico'
    },
    "WireShark": {
        'app_path': r'D://Wireshark//Wireshark.exe',
        'app_icon_path': r'RES//app_icon//wireshark.ico'
    },
    "MobaXterm": {
        'app_path': r'D://xterm//MobaXterm.exe',
        'app_icon_path': r'RES//app_icon//moba.ico'
    },
    "Source Insight": {
        'app_path': r'D://SourceInsight//sourceinsight4.exe',
        'app_icon_path': r'RES//app_icon//sourceinsight.ico'
    },
}