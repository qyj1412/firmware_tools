'''
Descripttion: 
Author: QYJ
version: 
Date: 2022-09-07 10:11:18
LastEditors: QYJ
LastEditTime: 2022-12-17 14:20:12
'''
from PySide6.QtCore import QObject, Slot
from PySide6.QtQml import QmlElement
from bat_manager import BatManager

QML_IMPORT_NAME = "io.qt.textproperties"
QML_IMPORT_MAJOR_VERSION = 1

@QmlElement
class Bridge(QObject):
    def __init__(self) -> None:
        super().__init__()
        self.bm = BatManager()
        
    @Slot(None, result='QVariant')
    def get_gitnames(self):
        return self.bm.GetGits()

    @Slot(str, result='QVariant')
    def get_scriptnames_by_gitname(self, gitname):
        return self.bm.GetBatMapByGit(gitname)

    @Slot(str, str, result=None)
    def run_script(self, gitname, scriptname):
        return self.bm.Exec(gitname, scriptname)

    @Slot(str, result=None)
    def run_app(self, appname):
        self.bm.RunApp(appname)

    @Slot(None, result='QVariant')
    def get_appnames(self):
        return self.bm.GetAppNames()   
    
    @Slot(None, result='QVariant')
    def get_appinfo(self):
        return self.bm.GetAppInfo()