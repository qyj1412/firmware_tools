'''
Descripttion: 
Author: QYJ
version: 
Date: 2022-09-06 14:13:24
LastEditors: QYJ
LastEditTime: 2022-12-17 14:19:49
'''
import os
from bat_path import Firmware_Path, App_Tool_Path

cmd_path = r"\\Tool\\FlashImage_produce"

def GetBatMap():
    bat_map = {}
    for key, val in Firmware_Path.items():
        bat_map[key] = Path2BatMap(val+cmd_path)

    return bat_map

def Path2BatMap(bat_path):
    bat_map = {}
    for i in os.listdir(bat_path):
        if os.path.splitext(i)[-1] == '.cmd':
            bat_map[os.path.splitext(i)[0]] = bat_path+ "\\" + i

    return bat_map

class BatManager:
    def __init__(self) -> None:
        self.bat_map = GetBatMap()
        
    def GetBatMap(self):
        bat_map_simple = {}

        for key, val in self.bat_map.items():
            bat_map_simple[key] = list(val.keys())

        return bat_map_simple

    def GetGits(self):
        return list(self.bat_map.keys())

    def GetBatMapByGit(self, git_name):
        return self.GetBatMap()[git_name]

    def Exec(self, git_name, cmd_name):
        os.chdir(os.path.dirname(self.bat_map[git_name][cmd_name]))
        os.system(self.bat_map[git_name][cmd_name])

    def GetAppNames(self):
        return list(App_Tool_Path.keys())
    
    def GetAppInfo(self):
        return App_Tool_Path

    def RunApp(self, appname):
        os.startfile(App_Tool_Path[appname]['app_path'])
        