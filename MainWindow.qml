import QtQuick 2.14
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.1
import QtQuick.Window 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Dialogs
import io.qt.textproperties 1.0

ApplicationWindow {
    id: root 
    width: 1000
    height: 600
    visible: true
    title: "固件生成脚本集合"
    Material.theme: Material.White

    Bridge {
        id: bridge
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("摩西摩西")
 
        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
			show_timer.start();
        }
		
		Timer {
			id: show_timer
			repeat: false
			running: false
			
			onTriggered: {
				messageDialog.close()
			}
		}
    }

    ComboBox {
        id: gitName

        anchors.top: parent.top
        anchors.left: parent.left
        

        model: []

        Component.onCompleted: { 
            model = bridge.get_gitnames()
            scriptView.model = bridge.get_scriptnames_by_gitname(gitName.currentText)
        }

        onActivated: {
            scriptView.model = bridge.get_scriptnames_by_gitname(currentText)
        }
    }

    ScrollView {
        id: scriptViewOut
        width: root.width / 3
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: gitName.right
        anchors.leftMargin: 10

        ListView {
            id: scriptView
            anchors.fill: parent
            
            model: []
            
            delegate: Button {
                width: scriptView.width
                height: scriptView.height/10
                text: modelData
                onClicked: bridge.run_script(gitName.currentText, text)
            }
            function setModel(new_model) {
                model = new_model
            }
        }
    }

    ScrollView {
        id: appListOut
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: scriptViewOut.right
        anchors.leftMargin: 50
        anchors.right: parent.right

        ListView {
            id: appListView
            anchors.fill: parent
            
            model: ListModel {

                ListElement {
                    app_name: ""
                    app_icon_path: ""
                }
            }
            
            Component.onCompleted: {
                // model = bridge.get_appnames()
                model.clear()

                var data = bridge.get_appinfo()
                for (var key in data)
                {
                    var item = data[key]
                    model.append({'app_name': key, 'app_icon_path': item['app_icon_path']})
                }
            }
            

            delegate: Rectangle {
                width: appListView.width
                height: appListView.height / 10
                color: "transparent"

                Rectangle {
                    id: img
                    anchors.left: parent.left
                    width: parent.height
                    height: parent.height
					color: "transparent"
					
                    Image {
                        anchors.fill: parent
                        
                        anchors.margins: 10
                        
                        source: app_icon_path

                        MouseArea {
                            anchors.fill: parent
                            
                            onDoubleClicked: { 
                                run_app(app_name)
                            }
                        }
                    }
                }

                Button {
                    width: parent.width - parent.height - 20
                    height: parent.height
                    text: app_name
                    anchors.left: img.right
                    anchors.leftMargin: 10

                    onClicked: run_app(text)
                }
            }
        }
    }

    function run_app(app_name)
    {
        bridge.run_app(app_name)
        messageDialog.show(app_name + " open succeed!!!")
    }

}